# Iptables

All knowledge about iptables to LPIc2 exam

- [Understading the basic commads](./iptables_commands.md)
- [Iptables as a service](./iptables_management.md)


# Introductioin NETFILTER/IPtables

"The netfilter hooks are a framework inside the Linux kernel that allows kernel modules to register callback functions at different locations of the Linux network stack. The registered callback function is then called back for every packet that traverses the respective hook within the Linux network stack". -- netfilter.org<br/>

So, the netfilter is implemented by kernel and iptables is a software that permit you management ipv4 and ipv6 firewall rules.

IPtables is composed of tables that it is composed of chains like image below

![](./img/tables.png)

![](./img/chains.png)

Every table has you own chains:<br/>
Filter has INPUT, OUTPUT and FORWARD<br/>
NAT has PREROUTING,POSTROUTING, and OUTPUT<br/>
Mangle has PREROUTING, OUTPUT, INPUT, FORWARD and POSTROUTING<br/>
Raw has PREROUTING and OUTPUT<br/>


# Bibliography
-[Netfilter.org](https://www.netfilter.org/)<br/>
-[iptables](https://linux.die.net/man/8/iptables)<br/>
