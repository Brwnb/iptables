<h1>The basics of iptables commands<h1>

<h2>This article must be used to understand iptables and how it works.</h2>
<h2>Never open ssh port to WAN. You need to use a VPN to access the ssh port</h2>

**Show the configured iptables rules**

`iptables -nL -t filter -> filter can be omitted like iptables -nL`<br/>
`iptables -nL -t nat`<br/>
`iptables -nL -t mangle`<br/>


**The basic of command options**

`iptables -t <filter/nat/mangle> -> again filter can be omitted`
`-n -> não resolve dns e portas`<br/>
`-A <chain> -> adiciona uma regra`<br/>
`-D <chain> -> Ele deleta uma regra`<br/>
`-I <chain> -> Ele adiciona acima.`<br/>
`-F <chain> -> to clean the chain rules`<br/>
`-P <chain> -> to change the allowed pattern to block pattern`<br/>
`--protocol  -p -> protocolo UDP, TCP, ICMP`<br/>
`--source -s -> ip de origem`<br/>
`--destination -d -> ip de destino`<br/>
`--in-interface -> -i nome da placa de rede -> para saída e forward`<br/>
`--out-interface -> -o para interface de loopback ou output`<br/>
`--jump -j -> ACCEPT, DROP, REJECT, RETURN, LOG, QUEUE, DNAT, SNAT... Vou aceitar ou vou recusar o pacote??`<br/>
`-v -> mostra detalhes das placas de rede`<br/>
`--dport -> destination port -> OBRIGATÒRIO o uso de -p tcp/udp`<br/>
`--sport -> source port`<br/>

**Cleaning the table pattern**

`iptables -F <INPUT/OUTPUT/FORWARD> -> to filter table`<br/>
`iptables -t filter -F OUTPUT -> it is another option because the filter can be omitted`<br/>

**Changing the tables state**

The pattern is everything accepted<br/>
`iptables -P INPUT DROP`
`iptables -P FORWARD DROP`


## IMPORTANTE
Devemos liberar alguma coisa antes de bloquear tudo.<br/>
Ex. Vou fazer a liberação do loopback e do ssh e bloquear o resto

`Iptables -A INPUT -i lo -j ACCEPT`<br/>
`Iptables -A OUTPUT -o lo -j ACCEPT`<br/>
`Iptables -A FORWARD -i lo -j ACCEPT`<br/>
`iptables -P INPUT DROP`<br/>
`iptables -P OUTPUT DROP`<br/>
`iptables -P FORWARD DROP`<br/>

Para não ter que digitar as mesmas regras tanto na entrada quanto na saída<br/>
Podemos utilizar os estados das conexões<br/>
Dessa maneira não há a necessidade de digitar a novamente a regra para o output <br/>
`iptables -A OUTPUT -m state --state (NEW),ESTABLISHED,RELATED -j ACCEPT`<br/>

**Firewall State**

This are about state of connectios. They increase the firewall performance because,<br/>
if the connection is allowed and the connection between client and server is finished,<br/>
the firewall won't use the CPU to manage the connection anymore, <br/>
The CPU will used just in the start connection.<br/>

## CAUTION
NEW should never to be used in **INPUT** or **FORWARD** because it to allowed new blocked connections.


# NAT
Official documentation about
[NAT](https://www.ietf.org/rfc/rfc3022.txt)

The NAT was created to prolong the ipv4 life.<br/>
He translate a private ip to public ip. The nat also is used for network isolate in a computing environment

There is two ways to create a nat in iptables<br/>
`iptables -t nat -A POSTROUTING -s ip.localnetwork/cidr.mask -o network.interface.with.the.public.ip -j SNAT --to-source public.ip.in.output.interface`<br/>
Ex: `iptables -t nat -A POSTROUTING -s 172.16.0.0/16 -o enp0s3 -j SNAT --to-source 100.254.254.254`

We can to use the MASQUERADE instead SNAT. it is common to use when you have a dynamic ip.<br/>
Ex: `iptables -t nat -A POSTROUTING -s 172.16.0.0/16 -o enp0s3 -j MASQUERADE`

**DNAT**

DNAT is used when you need to allow a connection from outside your network to a specific service inside your network.<br/>
We'll open a port in our firewall that will be redirect to a specific ip and port <br/>
`iptables -A PREROUTING -p tcp --dport 9999 -j DNAT --to-destination 172.16.0.2:22`

**The minimal rules to use iptables as a firewall**
```
iptables -t nat -A POSTROUTING -s 10.10.0.0/24 -o enp0s3 -j SNAT --to-source 192.168.254.221
iptables -A INPUT -p icmp -j ACCEPT
iptables -A INPUT -p udp -m udp --sport 53 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --sport 80 -j ACCEPT
iptables -A INPUT -s 192.168.254.224/32 -d 192.168.254.221/32 -p tcp -m tcp --dport 22 -j ACCEPT
iptables -A FORWARD -p icmp -j ACCEPT
iptables -A FORWARD -p udp -m udp --sport 53 -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 53 -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --sport 80 -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --sport 443 -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 443 -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o lo - ACCEPT
iptables -A OUTPUT -p udp -m udp --dport 53 -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT
iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -s 192.168.254.221/32 -d 192.168.254.224/32 -p tcp -m tcp --sport 22 -j ACCEPT
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP
```

**How to redirect 9999 port to 3389 port in the firewall**
```
iptables -A PREROUTING -p tcp --dport 9999 -j DNAT --to-destination 172.16.0.100:3389
iptables -A FORWARD -p tcp --dport 3389 -j ACCEPT
iptables -A FORWARD -p tcp --sport 3389 -j ACCEPT
```
