# Save the configuration in a file<br/>
iptables-save > firewall.rules<br/>
ip6tables-save > firewall.rules6<br/>

# Restore the configuration from file<br/>
iptables-restore < firewall.rules<br/>
ip6tables-save < firewall.rules6<br/>

# Save and restore at the boot <br/>
**Debian**<br/>
We need to install the packege iptables-persistent:<br/>
`apt-get install iptables-persistent`

To automatic restore the iptables rules we need to save in /etc/iptables/rules.v(4 or 6):<br/>
`iptables-save > /etc/iptables/rules.v4`<br/>
`ip6tables-save > /etc/iptables/rules.v6`<br/>

**CentOS**<br/>
`dnf install iptables-services`<br/>

To save the current rules we need execute the command:<br/>
`service iptables save`
